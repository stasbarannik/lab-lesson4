const readLine = require('readline');
const fs = require('fs');
const path = require('path');

const rl = readLine.createInterface({
   output: process.stdout,
   input: process.stdin
});

const fileBaseName = '/dist/data/record-';
const fileExtension = '.txt';
const fileAuditBaseName = './dist/audit/';
const fileAuditExtension = '.csv';
const questionsArray = [
    {
        question: 'What is your name: ',
        validationType: 'string',
        title: 'name',
    },
    {
        question: 'Choose type ("water" or "gas"): ',
        validationType: 'type',
        title: 'type',
    },
    {
        question: 'Enter the date (e.g. 07-13-2021): ',
        validationType: 'date',
        title: 'date',
    },
    {
        question: 'Enter the value: ',
        validationType: 'int',
        title: 'value',
    }
];

/**
 * Ask user a data and validate one
 *
 * @param q
 * @param type
 * @param errorMessage
 * @returns {Promise<unknown>}
 */
function askSync(q, type, errorMessage = 'Validation error, please enter valid data') {
    return new Promise(((resolve, reject) => {
        rl.question(q, (data) => {
            let value = data;
            switch (type) {
                case 'type':
                    if( value !== 'gas' && value !== 'water' ) {
                        console.log(errorMessage);
                        resolve(askSync(q, type, errorMessage));
                    } else {
                        console.log(data,'recived');
                        resolve(data);
                    }
                    break;

                case 'int':
                    if( parseInt(value) > 0 ) {
                        console.log(data,'recived');
                        resolve(data);
                    } else {
                        console.log(errorMessage);
                        resolve(askSync(q, type, errorMessage));
                    }
                    break;

                case 'date':
                    if( value.length === 10 ) { //TODO improve
                        console.log(data,'recived');
                        resolve(data);
                    } else {
                        console.log(errorMessage);
                        resolve(askSync(q, type, errorMessage));
                    }
                    break;

                default:
                    if( value.length > 0 ) {
                        console.log(data,'recived');
                        resolve(data);
                    } else {
                        console.log(errorMessage);
                        resolve(askSync(q, type, errorMessage));
                    }
                    break;
            }
        });
    }));
}

/**
 * Add a record of data
 * Run by default
 *
 * @returns {Promise<void>}
 */
async function asker() {
    let dateStamp = new Date().toJSON().slice(0, 19).replace(/:/g, "-"),
        resultData = {};

    for (let i = 0; i < questionsArray.length; i++){
        resultData[questionsArray[i].title] = await askSync(questionsArray[i].question, questionsArray[i].validationType);
    }

    //All question done
    rl.close();

    filePath = path.join(__dirname, fileBaseName + dateStamp + fileExtension)

    fs.appendFile(filePath, JSON.stringify(resultData),{encoding: "utf-8"}, (err) => {
        if (err) throw err;
    });

    console.log("Your data stored successful");
}

/**
 * Is file exist helper
 *
 * @param filepath
 * @returns {boolean}
 */
function checkFileExistsSync(filepath){
    let flag = true;
    try{
        fs.accessSync(filepath, fs.constants.F_OK);
    }catch(e){
        flag = false;
    }
    return flag;
}

/**
 * Add the data from records to general base
 * Run by default
 *
 * @returns {Promise<void>}
 */
async function watcher () {
    fs.watch(path.join(__dirname, './dist/data'), (eventType, fileName) => {
        if( checkFileExistsSync(filePath) ) {
            const filePath = path.join(__dirname, './dist/data', fileName);
            const content = fs.readFileSync(filePath, {flag:'r', encoding: "utf-8"}).toString();
            if(content.length > 0) {
                const parsedData = JSON.parse(content);
                const fileAuditPath = fileAuditBaseName + parsedData.type + fileAuditExtension;

                let resultData = parsedData;
                delete resultData.type;

                fs.appendFile(path.join(__dirname, fileAuditPath),
                    JSON.stringify(resultData) + ",", //Add comma to read all data as JSON
                    () => {
                        console.log("audit data added");
                        fs.unlink(filePath, (err) => {
                            if (err) throw err;
                            console.log('source file was deleted');
                        });
                    });
            }
        }
    });
}

/**
 * Display audit
 * Run by node insert-data.js audit {type} (e.g. gas or water)
 *
 * @param reportType
 * @returns {Promise<void>}
 */
async function audit (reportType) {
    const fileAuditPath = fileAuditBaseName + reportType + fileAuditExtension;
    let content = fs.readFileSync(fileAuditPath, {flag:'r', encoding: "utf-8"}).toString();
    if(content.length > 0) {
        content = "[\n" + content.slice(0, -1) + "\n]"; //Removing last comma and adding [] to make JSON valid
        const parsedData = JSON.parse(content);

        let map = {};

        parsedData.forEach( (val) => {
            if( typeof map[val.name] === "undefined" ) {
                map[val.name] = [];
            }
            map[val.name].push({date: val.date, value: val.value});
        });

        Object.keys(map).forEach(function(user) {
            console.log("User " + user + " added data " + map[user].length + " times.");
            console.log("=============== Details ================");
            map[user].forEach( (item) => {
                console.log(item['date'] + ": " + item['value']);
            });
            console.log("========== End of user datails =++======");
            console.log("");
        });
    }
}



/**
 * Routing
 */
// Run node insert-data.js audit gas or node insert-data.js audit water to see the audit result
if( process.argv.length > 2 && process.argv[2] === 'audit' ) {
    if( process.argv.length > 3 && process.argv[3].toString().length > 0 ) {
        audit(process.argv[3].toString());
    } else {
        console.log("Please enter audit type, e.g. gas or water");
    }
} else {
    // By default cases script will ask you for new record data
    asker();
    watcher();
}


